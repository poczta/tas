# Generated by Django 4.0.4 on 2022-04-21 18:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('transport', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='devicetype',
            name='devicetype_name',
            field=models.CharField(max_length=30, verbose_name='Device Type Name'),
        ),
    ]
