from django.contrib.auth.models import User
from django import forms
from .models import fuel
from .models import work
from .models import cost
from .models import daily


class DateInput(forms.DateInput):
    input_type = 'datetime-local'


class PostFormFuel(forms.ModelForm, ):
    class Meta:
        model = fuel
        ifadmin = 2
        title = forms.CharField()
        description = forms.CharField()
        first_name = forms.CharField(max_length=200)
        last_name = forms.CharField(max_length=200)
        roll_number = forms.IntegerField(help_text="Enter 6 digit roll number")
        password = forms.CharField(widget=forms.PasswordInput())

        if (1 == ifadmin):
            fields = ('fuel_registration', 'fuel_amont',
                      'fuel_odometr',  'fuel_location', 'fuel_type')
        else:
            fields = ('fuel_amont', 'fuel_odometr',
                      'fuel_location', 'fuel_type')


class PostFormWork(forms.ModelForm):
    class Meta:
        model = work
        fields = ('work_odometr', 'work_location',
                  'work_type', 'work_cancelled')


class PostFormCost(forms.ModelForm):
    class Meta:
        model = cost
        fields = ('cost_amont', 'cost_type', 'cost_currency', 'cost_Main_Img')


class PostFormDaily(forms.ModelForm):
    class Meta:
        model = daily
        fields = ("__all__")
