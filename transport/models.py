import datetime
from pickle import TRUE
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import logging






class worktype(models.Model):
    worktype_sysid = models.AutoField(primary_key=True)
    worktype_name = models.CharField(max_length=30, verbose_name=(_('Work Type Name')))
    worktype_status = models.IntegerField(_('Status'))

    def __str__(self):
        return self.worktype_name

class dailytype(models.Model):
    dailytype_sysid = models.AutoField(primary_key=True)
    dailytype_name = models.CharField(max_length=30, verbose_name=(_('Daily Type Name')))
    dailytype_status = models.IntegerField(_('Status'))
    dailytype_locale = models.CharField(max_length=30, verbose_name=(_('Daily Type Locale')))

    def __str__(self):
        return 'Nazwa={0}, Status={1}, Locale={2}'.format(self.dailytype_name, self.dailytype_status, self.dailytype_locale)

class devicetype(models.Model):
    devicetype_sysid = models.AutoField(primary_key=True)
    devicetype_name = models.CharField(max_length=30, verbose_name=(_('Device Type Name')))
    devicetype_status = models.IntegerField(_('Status'))

    def __str__(self):
        return self.devicetype_name

class fueltype(models.Model):
    fueltype_sysid = models.AutoField(primary_key=True)
    fueltype_name = models.CharField(max_length=30)
    fueltype_status = models.IntegerField(_('Status'))
    fueltype_locale = models.CharField(max_length=3, verbose_name=(_('Fuel Type Locale')))

    def __str__(self):
        return self.fueltype_name

class costtype(models.Model):
    costtype_sysid = models.AutoField(primary_key=True)
    costtype_name = models.CharField(max_length=30, verbose_name=(_('Cost Type Name')))
    costtype_status = models.IntegerField(_('Status'))
    costtype_locale = models.CharField(max_length=3, verbose_name=(_('Cost Type Locale')))

    def __str__(self):
        return self.costtype_name

class currencytype(models.Model):
    currency_sysid = models.AutoField(primary_key=True)
    currency_name = models.CharField(max_length=30, default='PLN', verbose_name=(_('Currency Name')))
    currency_status = models.IntegerField(_('Status'))

    def __str__(self):
        return self.currency_name


class device(models.Model):
    device_sysid = models.AutoField(primary_key=True)
    device_registration = models.CharField(max_length=10)
    device_status = models.IntegerField('Status')
    device_assigned =models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=(_('Assigend')))
    device_vin = models.CharField(max_length=50)
    device_date_tech = models.DateTimeField('Tech Date')
    device_date_oil = models.DateTimeField('Oil Date')
    device_oddometr = models.IntegerField('Oddometr')
    device_type = models.ForeignKey(devicetype, on_delete=models.CASCADE, verbose_name=(_('Device Type')))

    def __str__(self):
        return self.device_registration





class fuel(models.Model):
    fuel_sysid = models.AutoField(primary_key=True)
    fuel_registration = models.ForeignKey(device, on_delete=models.CASCADE, verbose_name=(_('Fuel Registration')))
    fuel_username = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=(_('Fuel Username')))
    fuel_amont = models.IntegerField(_('Fuel amount'))
    fuel_odometr = models.IntegerField(_('Oddometr'))
    fuel_location = models.CharField(max_length=50, verbose_name=(_('Fuel Location')))
    fuel_type = models.ForeignKey(fueltype, on_delete=models.CASCADE, verbose_name=(_('Fuel Type')))
    fuel_date = models.DateTimeField(_('Fuel Date'))

    def save(self, *args, **kw):
        if not self.fuel_date:
            self.fuel_date = timezone.now()
        super(fuel, self).save(*args, **kw)

    def was_published_recently(self):
        return self.fuel_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return 'Kierowca={0}, Rejestracja={1}'.format(self.fuel_username, self.fuel_registration)


class work(models.Model):
    work_sysid = models.AutoField(primary_key=True)
    work_registration = models.ForeignKey(device, on_delete=models.CASCADE, verbose_name=(_('registration')))
    work_username = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name=(_('Username')))
    work_city = models.CharField(max_length=50, verbose_name=(_('City')))
    work_country = models.CharField(max_length=50, verbose_name=(_('Country')))
    work_postal = models.CharField(max_length=10, verbose_name=(_('Work Postal')))
    work_odometr = models.IntegerField(_('Oddometr'))
    work_location = models.CharField(max_length=50 , verbose_name=(_('Work Location')))
    work_type = models.ForeignKey(worktype, on_delete=models.CASCADE, verbose_name=(_('Work Type')))
    work_date = models.DateTimeField(_('Work Date'))
    work_cancelled = models.IntegerField(_('Cancelled'))
    work_gps_long = models.CharField(max_length=50, verbose_name=(_('Work GPS Long')))
    work_gps_lati = models.CharField(max_length=50, verbose_name=(_('Work GPS Lati')))

    def save(self, *args, **kw):
        if not self.work_date:
            self.work_date = timezone.now()
        super(work, self).save(*args, **kw)


    def was_published_recently(self):
        return self.work_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return 'Kierowca={0}, Rejestracja={1}, Rodzaj={2}'.format(self.work_username, self.work_registration, self.work_type)


class cost(models.Model):
    cost_sysid = models.AutoField(primary_key=True)
    cost_registration = models.ForeignKey(device, on_delete=models.CASCADE)
    cost_username = models.ForeignKey(User, on_delete=models.CASCADE)
    cost_amont = models.IntegerField(_('Cost amount'))
    cost_type = models.ForeignKey(costtype, on_delete=models.CASCADE, verbose_name=(_('Cost Type Name')))
    cost_date = models.DateTimeField(_('Cost Date'))
    cost_currency = models.ForeignKey(currencytype, on_delete=models.CASCADE, verbose_name=(_('Currency')))
    cost_Main_Img = models.ImageField(upload_to='costs/')
    cost_status = models.CharField(max_length=50, verbose_name=(_('Cost Status')), default='Nie')


    def save(self, *args, **kw):
        if not self.cost_date:
            self.cost_date = timezone.now()
      

        super(cost, self).save(*args, **kw)

    def was_published_recently(self):
        return self.cost_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return 'Kierowca={0}, Rejestracja={1}'.format(self.cost_username, self.cost_registration)


class daily(models.Model):

    def sample_view(request):
        current_user = request.user
        reg = device.objects.create(device_assigned=current_user)
        return reg


    assigened  =1
    daily_sysid = models.AutoField(primary_key=True)
    daily_registration = models.ForeignKey(device, on_delete=models.CASCADE,  default=assigened)
    daily_username = models.ForeignKey(User, on_delete=models.CASCADE, default=2)
    daily_name = models.ForeignKey(dailytype, on_delete=models.CASCADE, verbose_name=(_('Daily Type Name')))
    daily_date = models.DateTimeField(verbose_name=(_('Daily Date')), default=timezone.now())
    daily_status = models.CharField(max_length=50, verbose_name=(_('Daily Status')), default='Tak')
    
    def save(self, *args, **kw):
        if not self.daily_date:
            self.daily_date = timezone.now()
      

        super(daily, self).save(*args, **kw)

    def was_published_recently(self):
        return self.daily_date >= timezone.now() - datetime.timedelta(days=1)

    def __str__(self):
        return 'Kierowca={0}, Rejestracja={1}, Status={2}'.format(self.daily_username, self.daily_registration , self.daily_status)