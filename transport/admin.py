from django.contrib import admin
from .models import device
from .models import fuel
from .models import work
from .models import worktype
from .models import fueltype
from .models import cost
from .models import costtype
from .models import currencytype
from .models import devicetype
from .models import dailytype
from .models import daily


admin.site.register(device)
admin.site.register(fuel)
admin.site.register(work)
admin.site.register(worktype)
admin.site.register(fueltype)
admin.site.register(cost)
admin.site.register(costtype)
admin.site.register(currencytype)
admin.site.register(devicetype)
admin.site.register(dailytype)
admin.site.register(daily)

# Register your models here.
