from django.shortcuts import render
from django.shortcuts import redirect
from .models import fuel, work, cost, device, daily, dailytype
from django.shortcuts import render, get_object_or_404
from .forms import PostFormFuel, PostFormWork, PostFormCost, PostFormDaily
from django.utils.translation import gettext as _
from django.contrib.auth.decorators import login_required
from django.db.models import Avg
from django.db import models
 


@login_required()
def index(request):
    latest_question_list = fuel.objects.order_by('-fuel_date')[:5]
    context = {
        'fueling': _('fueling'),
        'work': _('work'),
        'cost': _('cost'),
        'daily': _('daily'),
        'latest_question_list': latest_question_list}
    return render(request, 'transport/index.html', context)




# ------------------------- fuel ----------------------------------
@login_required() 
def fuel_list(request):
    fuels = fuel.objects.all().filter(fuel_username=request.user).order_by('-fuel_date')[:5]
    return render(request, 'transport/fuel/fuel_list.html', {'fuels': fuels})

@login_required() 
def fuel_detail(request, pk):
    post = get_object_or_404(fuel, pk=pk)
    return render(request, 'transport/fuel/fuel_detail.html', {'post': post})

@login_required() 
def fuel_new(request):
    
    if request.method == "POST":

        form = PostFormFuel(request.POST)
        if form.is_valid():
            post = form.save(commit=False)

       

            device_registration = get_object_or_404(device, device_assigned=request.user.id)
            post.fuel_registration = device_registration
            post.fuel_username_id = request.user.id
            
            post.save()

            return redirect('fuel_list')
    else:
        form = PostFormFuel()
    return render(request, 'transport/fuel/fuel_add.html', {'form': form})
# ------------------------- work ----------------------------------

@login_required() 
def work_list(request):
    works = work.objects.all().filter(work_username=request.user).order_by('-work_date')[:5]
    return render(request, 'transport/work/work_list.html', {'works': works})

@login_required() 
def work_new(request):
    if request.method == "POST":
        form = PostFormWork(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            work_registration = get_object_or_404(device, device_assigned=request.user.id)
            post.work_registration = work_registration
            post.work_username_id = request.user.id
            post.save()
            return redirect('work_list')
    else:
        form = PostFormWork()
    return render(request, 'transport/work/work_add.html', {'form': form})

@login_required() 
def work_detail(request, pk):
    post = get_object_or_404(work, pk=pk)
    return render(request, 'transport/work/work_detail.html', {'post': post})

# ------------------------- costs ----------------------------------

@login_required() 
def cost_list(request):
    costs = cost.objects.all().filter(cost_username=request.user).order_by('-cost_date')[:5]
    return render(request, 'transport/cost/cost_list.html', {'costs': costs})

@login_required() 
def cost_new(request):
    
    if request.method == "POST":
        form = PostFormCost(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.cost_username_id = request.user.id
            cost_registration = get_object_or_404(device, device_assigned=request.user.id)
            post.cost_registration = cost_registration
            post.save()
            return redirect('cost_list')
 
    else:
        form = PostFormCost()
    return render(request, 'transport/cost/cost_add.html', {'form': form})

@login_required() 
def cost_detail(request, pk):
    costs = get_object_or_404(cost, pk=pk)
    return render(request, 'transport/cost/cost_detail.html', {'cost': costs})

# ------------------------- daily ----------------------------------

@login_required() 
def daily_list(request):
    dailys = daily.objects.extra({'daily_date':"date(daily_date)"}).values('daily_date' , 'daily_sysid').annotate(count=Avg('daily_sysid'))
    #dailys = dailytype.objects.all().filter(dailytype_status=1)[:5]
    return render(request, 'transport/daily/daily_list.html', {'dailys': dailys})


@login_required() 
def daily_new(request):
    
    if request.method == "POST":
        form = PostFormDaily(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.daily_username = request.user.id
            daily_registration = get_object_or_404(device, device_assigned=request.user.id)
            post.daily_registration = daily_registration
            post.save()
            return redirect('daily_list')
 
    else:
        form = PostFormDaily()
    return render(request, 'transport/daily/daily_add.html', {'form': form})

@login_required() 
def daily_detail(request, pk):
    dailys = get_object_or_404(daily, pk=pk)
    return render(request, 'transport/daily/daily_detail.html', {'daily': dailys})