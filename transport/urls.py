from django.urls import path
from . import views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from .views import *

urlpatterns = [
    path('', views.index, name='transport'),
    path('', views.index, name='index'),
    path('work/', views.work_list, name='work_list'),
    path('work/new', views.work_new, name='work_new'),
    path('work/<int:pk>/', views.work_detail, name='work_detail'),

    path('fuel/', views.fuel_list, name='fuel_list'),
    path('fuel/new', views.fuel_new, name='fuel_new'),
    path('fuel/<int:pk>/', views.fuel_detail, name='fuel_detail'),

    path('cost/', views.cost_list, name='cost_list'),
    path('cost/new', views.cost_new, name='cost_new'),
    path('cost/<int:pk>/', views.cost_detail, name='cost_detail'),

    path('daily/', views.daily_list, name='daily_list'),
    path('daily/new', views.daily_new, name='daily_new'),
    path('daily/<int:pk>/', views.daily_detail, name='daily_detail'),


   

]

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL,
                              document_root=settings.MEDIA_ROOT)
